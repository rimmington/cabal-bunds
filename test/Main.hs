module Main (main) where

import Data.Version (Version (..), parseVersion)
import Distribution.Bunds (addBounds, dropBounds)
import RIO
import RIO.List (headMaybe)
import Test.Hspec
import Text.ParserCombinators.ReadP (readP_to_S)

main :: IO ()
main = hspec spec

spec :: Spec
spec = do
    describe "parseVersion" do
        it "parses a version" do
            headMaybe (reverse (readP_to_S parseVersion "0.1.0.0")) `shouldBe`
                Just (Version [0, 1, 0, 0] [], "")
    describe "dropBounds" do
        it "works on fixture 1" do
            input <- readFileUtf8 "test/fixtures/drop1.cabal"
            expected <- readFileUtf8 "test/fixtures/dropped1.cabal"
            dropBounds input `shouldBe` ([], expected)
    describe "addBounds" do
        it "works on fixture 1" do
            input <- readFileUtf8 "test/fixtures/dropped1.cabal"
            expected <- readFileUtf8 "test/fixtures/drop1.cabal"
            addBounds input `shouldReturn` ([], expected)
