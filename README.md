# cabal-bunds

Simple and incomplete bounds management for [Cabal files](https://cabal.readthedocs.io/en/stable/), at least as long as Cabal [doesn't have an exact-printer](https://github.com/haskell/cabal/issues/7544).

`cabal-bunds-drop` removes `^>=` style bounds from a Cabal file, and `cabal-bunds-add` adds `^>=` style bounds to any package without a bound based on the current package environment (via `ghc-pkg list`).

Only lines with leading commas are supported.
