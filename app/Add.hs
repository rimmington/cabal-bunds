module Main (main) where

import Data.Version (showVersion)
import Distribution.Bunds (Warning (..), addBounds)
import Options.Applicative.Simple (empty, metavar, simpleOptions, strArgument)
import Paths_cabal_bunds (version)
import RIO
import RIO.Text qualified as T
import System.IO (putStrLn)

main :: IO ()
main = do
    (opts, ()) <- simpleOptions
        (showVersion version)
        "cabal-bunds-add"
        "Add versions based on current ghc-pkg database"
        (strArgument (metavar "CABAL_FILE"))
        empty
    input <- readFileUtf8 opts
    (ws, output) <- addBounds input
    traverse_ (\(Warning w) -> putStrLn (T.unpack w)) ws
    writeFileUtf8 opts output
