{-# language OverloadedStrings #-}

module Distribution.Bunds (Warning (..), addBounds, dropBounds) where

import Data.Tuple (swap)
import Data.Version (Version, parseVersion, showVersion)
import RIO
import RIO.ByteString.Lazy qualified as BL
import RIO.Char (isAlpha, isSpace)
import RIO.HashMap qualified as HM
import RIO.Text qualified as T
import RIO.Text.Partial qualified as T
import RIO.Writer (runWriter, tell)
import System.Process.Typed (proc, readProcessStdout)
import Text.ParserCombinators.ReadP (readP_to_S)

newtype Warning
  = Warning Text
  deriving stock (Eq, Show)
  deriving newtype (Hashable)

dropBounds :: Text -> ([Warning], Text)
dropBounds = fmap T.unlines . swap . runWriter . mapBuildDependsLine drp . T.lines
  where
    drp l = (startL <>) <$> endL
      where
        (T.stripEnd -> startL, restL) = T.breakOn "^>=" l
        endL = case restL of
            "" -> pure ""
            r
              | (vt:rst) <- T.words $ T.drop 3 r
              , Just _ <- parseVersion1 vt
              -> pure $ T.unwords rst
            _ -> restL <$ tell [Warning $ "Not stripping version from complex line '" <> T.strip l <> "'"]

addBounds :: Text -> IO ([Warning], Text)
addBounds full = do
    (exitCode, output) <- readProcessStdout $ proc "ghc-pkg" ["list", "--simple-output"]
    unless (exitCode == ExitSuccess) $ throwIO GhcPkgFailed
    pure . fmap T.unlines . swap $ runWriter do
        pkgs <- HM.fromList . catMaybes <$>
            traverse readPkg (T.words (decodeUtf8Lenient (BL.toStrict output)))
        let ls = T.lines full
            (_, nameAndRest) = break ("name:" `T.isInfixOf`) ls
        thisPkg <- case nameAndRest of
            (l:_)
              | (w:_) <- reverse (T.words l)
              -> pure $ Just w
            _ -> Nothing <$ tell [Warning "Could not determine the name of this package"]
        mapBuildDependsLine (go pkgs thisPkg) ls
  where
    readPkg pt
      | (p, vt) <- T.breakOnEnd "-" pt
      , Just v <- parseVersion1 vt
      = pure $ Just (T.dropEnd 1 p, v) -- drop trailing -
      | otherwise
      = Nothing <$ tell [Warning $ "Cannot determine version of package '" <> pt <> "'"]
    go pkgs thisPkg l = case reverse . T.words $ clean l of
        (w:_)
          | w == "build-depends:"
          -> pure l
          | Just (hc, _) <- T.uncons w
          , isAlpha hc
          , Just w /= thisPkg
          -> case HM.lookup w pkgs of
            Nothing -> l <$ tell [Warning $ "Did not find '" <> w <> "' in ghc-pkg list"]
            Just v  -> pure $ T.replace w (w <> " ^>=" <> T.pack (showVersion v)) l
        _ -> pure l
    clean = T.dropAround (\c -> isSpace c || c == ',') . commentless

parseVersion1 :: Text -> Maybe Version
parseVersion1 vt
  | ((v, ""):_) <- reverse $ readP_to_S parseVersion (T.unpack (T.strip vt))
  = Just v
  | otherwise
  = Nothing

mapBuildDependsLine :: Applicative f => (Text -> f Text) -> [Text] -> f [Text]
mapBuildDependsLine f = go
  where
    go [] = pure []
    go ls = (<>) <$> ((start <>) <$> traverse f mid) <*> go end
      where
        (start, rest) = break buildDepends ls
        (mid, end) = first (take 1 rest <>) $ break isNotDepends (drop 1 rest)
        buildDepends t = "build-depends" `T.isPrefixOf` T.stripStart t
        isNotDepends t = maybe False (isAlpha . fst) (T.uncons t') || T.any (== ':') t'
          where
            t' = commentless t

commentless :: Text -> Text
commentless = T.strip . fst . T.breakOn "--"

data GhcPkgFailed = GhcPkgFailed deriving (Exception, Show)
